<?php 
	require_once("conn.php");
	 
	if (isset($_POST['submit'])) {
		$atelier = $_POST['atelier'];
		$nom = $_POST['nom'];
		$prenom = $_POST['prenom'];
        $tel = $_POST['tel'];
		$sexe = $_POST['sexe'];
		$niveau = $_POST['niveau'];
		$connaissance = $_POST['connaissance'];
       // $commentaire = $_POST['commentaire'];
	
        
        
    $insertion = $conn->prepare("INSERT INTO inscription(atelier_inscri, nom_inscri, prenom_inscri, num_inscri, sex_inscri, niveau_inscri, connaissance_inscri) value(?,?,?,?,?,?,?)");
    $insertion->execute(array($atelier, $nom, $prenom, $tel, $sexe, $niveau, $connaissance)); 

	/*	$ps = $conn->prepare("INSERT INTO visiteur (atelier_visit, nom_visit, prenom_visit, sexe_visit, niveau_visit, connaissance_visit, commentaire_visit) value(?,?,?,?,?,?,?)");
		$insert = array($atelier,$nom,$prenom,$sexe,$niveau,$connaissance,$commentaire);
		$ps->execute($insert);

		header("location:formulaire.php");*/	
	}


?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>INSCRIPTION</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link href="css/animate.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">      
	<link href="css/style.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
    <!-- =======================================================
        Theme Name: Gp
        Theme URL: https://bootstrapmade.com/gp-free-multipurpose-html-bootstrap-templat/
        Author: BootstrapMade
        Author URL: https://bootstrapmade.com
    ======================================================= -->  
    
</head>
  <body class="homepage">   
	<header id="header">
        <nav class="navbar navbar-fixed-top" role="banner">
            <div class="container">
              <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href=""><b><img src="images/logoCamp.png" width="50" height="50">Camp Numerique Ngouoni</b></a>
              </div>
				<form action="" method="post" action="affichageBD.php" role="form" class="contactForm">
                <div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <li><a href="inscription.php">inscrir</a></li>
                        <li><a href="affichageBD.php?test=html">HTML</a></li>
                        <li><a href="affichageBD.php?test=montage">Montage Video</a></li>
                        <li><a href="affichageBD.php?test=infographie">Infographie</a></li>    
                        <li><a href="affichageBD.php?test=md">Modelisation3D</a></li>  
                        <li class=""><a href="affichageBD.php?test=otp">Outils De prod</a></li>
                         <li><a href="affichageBD.php?test=tout">inscrits</a></li>  
                    </ul>
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->
        </form>
		
    </header><!--/header-->
		
	<div class="map">
	</div>
	</div>
    <br>
	
	<section id="contact-page">
        <div class="container">
            <div class="center">        
                <h2>Inscription au Camp Numerique Ngouoni</h2>
                 <img src="images/Sans titre - 9.jpg" width="300" height="100">
                <p class="lead"></p>
            </div> 
            <div class="row contact-wrap"> 
                <div class="col-sm-8 col-sm-offset-2">
                    <div id="sendmessage">Your message has been sent. Thank you!</div>
                    <div id="errormessage"></div>
                       <div class="form-group">
                      
                    <form action="" method="post" action="#" role="form" class="contactForm">
                        <div class="form-group">
                            Atelier
                            <select name="atelier" required="">
                                <option>Infographie</option>
                                <option>Modélisation 3D</option>
                                <option>Outils de productivité</option>
                                <option>Programmation & HTML</option>
                                <option>Montage Vidéo</option>
                            </select> <div class="validation">
                                </div><br/><br>
                                <div class="form-group">
                                    Nom:<input type="text" name="nom" class="form-control" id="name" placeholder="" required data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                                    <div class="validation"></div>
                                </div>
                                 Prénom:<input type="text" name="prenom" class="form-control" id="name" placeholder="" required data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                                    <div class="validation"></div><br>


                                 <div class="form-group">
                                    Numéro:<input type="text" name="tel" class="form-control" id="name" placeholder="" required data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                                    <div class="validation"></div>
                                </div>


                            </div><br/>
                                <label>Sexe
                 <p>
          <label>
            <input type="radio" name="sexe" value="homme" required id="RadioGroup1_0" />
            Masculin</label>
          <label>
            <input type="radio" name="sexe" value="femme" required id="RadioGroup1_1" />
            Feminin</label>
        </p>
                 </label>

                        <div class="form-group">
                                Niveau d'Etude
                                    <select name="niveau">
                                                <option>1er Cycle</option>
                                                <option>2nd Cycle</option>
                                                <option>Autres</option>
                                    </select> <div class="validation">
                </div>
                 <br/> <br>

                         <div class="form-group">
                               Connaissance en Informatique
                                     <select name="connaissance">
                                            <option>Faible</option>
                                            <option>Moyenne</option>
                                            <option>Elevé</option>
                                    </select> <div class="validation">
                </div>

                                    <div class="validation"></div>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" name="commentaire" rows="5" data-rule="" data-msg="Please write something for us" placeholder="Commentaire"></textarea>
                                    <div class="validation"></div>
                                </div>

                                <div class="text-center"><button type="submit" name="submit" class="btn btn-primary btn-lg">Envoyer</button></div>
                    
                    </form>     <br> <br>
                        
                        
                      
                </div>
            </div><!--/.row-->
        </div><!--/.container-->
    </section><!--/#contact-page-->
            
            <!-- <form action="" method="post" action="affichageBD.php" role="form" class="contactForm">
               <div class="text-center"><button type="submit" name="infographie" class="btn btn-primary btn-lg">infographie</button></div>
                        <div class="text-center"><button type="submit" name="html" class="btn btn-primary btn-lg">html</button></div>
                        <div class="text-center"><button type="submit" name="productivite" class="btn btn-primary btn-lg">outils de productivite</button></div>
                        <div class="text-center"><button type="submit" name="troisD" class="btn btn-primary btn-lg">Modelisation 3D</button></div>
                        <div class="text-center"><button type="submit" name="montage" class="btn btn-primary btn-lg">Montage video</button></div>
            </form>-->
            
            
<?php


/*if(isset($_POST['montage'])){

require_once("conn.php");
$req = $conn->query("SELECT nom_inscri, prenom_inscri, atelier_inscri FROM inscription WHERE atelier_inscri='Montage Vidéo'");
$donnees = $req->fetch();




    echo "<h1> Liste des inscrits en montage video </h1> <br> <br>";


while ($donnees = $req->fetch()){


    
    
   echo $donnees['nom_inscri'] . " |" . $donnees['prenom_inscri'] ." |". $donnees['atelier_inscri']  ."<br>";
}
    
         }

/*require_once("conn.php");
$req = "SELECT * FROM inscription WHERE atelier_inscri = 'Montage Vidéo'";
$ps = $conn->prepare($req);
$ps -> execute();

'SELECT * FROM inscription WHERE atelier_inscri=\'Montage Vidéo\''*/


?>
            
            
            
            

  <section id="bottom">
        <div class="container wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="widget">
                        
                    </div>    
                </div><!--/.col-md-3-->

                <div class="col-md-3 col-sm-6">
                    <div class="widget">
                        <h3> TSOUMOU </h3>
                        <img src="images/tsoumou.png" width="100" height="100">
                       <!-- <ul>
                            <li><a href="#">BP : 20 277-Libreville</a></li>
                            <li><a href="#">+24106 02 89 47 81</a></li>
                            <li><a href="#"> www.kaytechnologie.com </a></li>
                            <li><a href="#"> infos@kaytechnologie.com </a></li>                           
                        </ul>-->
                    </div>    
                </div><!--/.col-md-3-->

                <div class="col-md-3 col-sm-6">
                    <div class="widget">
                        <h4>Prestataire</h4>
                        <ul>
                            <li><a href="https://www.facebook.com/profile.php?id=100011192410168"><img src="images/logoKay.jpg" width="60" height="60">KAYTECHNOLOGIE</a></li>
                                                  
                        </ul>
                    </div>    
                    
                     <div class="widget">
                        <h5></h5>
                        <ul>
                            <li></a></li>                           
                        </ul>
                    </div>    
                </div><!--/.col-md-3-->
                
                
                 <div class="col-md-3 col-sm-6">
                    <div class="widget">
                        <h4>Partenaire</h4>
                        <ul>
                            <li><a href="https://www.facebook.com/allmultimediaservices/"><img src="images/allmultimediaservices%20logo1.jpg" width="60" height="50">Allmultimediaservice</a></li>
                                                  
                        </ul>
                    </div>    
                    
                     <div class="widget">
                        <h5></h5>
                        <ul>
                                                  
                        </ul>
                    </div>    
                </div><!--/.col-md-3-->

                <div class="col-md-3 col-sm-6">
                    <div class="widget">
                    </div>    
                </div><!--/.col-md-3-->
            </div>
        </div>
    </section><!--/#bottom-->
	
	<div class="top-bar">
		<div class="container">
			<div class="row">
			    <div class="col-lg-12">
				   <div class="social">
						<ul class="social-share">
							<li><a href="https://web.facebook.com/search/top/?q=camp%20numerique%20ngouoni"><i class="fa fa-facebook"></i></a></li>
						</ul>
				   </div>
                </div>
			</div>
		</div><!--/.container-->
	</div><!--/.top-bar-->
	
	<footer id="footer" class="midnight-blue">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="credits">
                        <!-- 
                            All the links in the footer should remain intact. 
                            You can delete the links only if you purchased the pro version.
                            Licensing information: https://bootstrapmade.com/license/
                            Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Gp
                        -->
                   by Kaytechnologie
                    </div>
                </div>
                <div class="col-sm-6">
                    <ul class="pull-right">
                        
                        <li><a href="blog.html">J'apprends le TEKE</a></li>
                    
                    </ul>
                </div>
            </div>
        </div>
    </footer><!--/#footer-->
	
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>   
    <script src="js/wow.min.js"></script>
	<script src="js/main.js"></script>
    
</body>
</html>
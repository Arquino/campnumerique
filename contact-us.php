<?php 
	require_once("conn.php");
	
	/*if (isset($_POST['valider'])) {
		$nom = $_POST['nom'];
		$email = $_POST['email'];
		$message = $_POST['message'];

		$ps = $conn->prepare("INSERT INTO contact (nom,email,message) value(?,?,?)");
		$insert = array($nom,$email,$message);
		$ps->execute($insert);

		header("location:contact-us.php");	
	}*/


if(isset($_POST['submit'])){
    
        $nom = $_POST['nom'];
		$email = $_POST['email'];
		$message = $_POST['message'];
    
    

	$insertion = $conn->prepare("INSERT INTO contact(nom_contact, email_contact, message_contact) VALUES (?,?,?)");
	$insertion->execute(array($nom, $email, $message)); 

        header("location:contact-us.php");	
	
}

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Camp_contact</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link href="css/animate.min.css" rel="stylesheet">
    <link href="css/prettyPhoto.css" rel="stylesheet">      
	<link href="css/style.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
    <!-- =======================================================
        Theme Name: Gp
        Theme URL: https://bootstrapmade.com/gp-free-multipurpose-html-bootstrap-templat/
        Author: BootstrapMade
        Author URL: https://bootstrapmade.com
    ======================================================= -->  
    
</head>
  <body class="homepage">   
	<header id="header">
        <nav class="navbar navbar-fixed-top" role="banner">
            <div class="container">
              <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html"><b> <img src="images/logoCamp.png" width="50" height="50"> Camp Numerique Ngouoni</b></a>
              </div>
				
                    <div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <li><a href="index.html">Accueil</a></li>
                        <li><a href="camp_numerique.html">Formation</a></li>
                        
                        
                        
                        <li class="dropdown">
				            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span data-hover="Short Codes"><span>Parlons Teke</span></span> <b class="caret"></b></a>
				            <ul class="dropdown-menu agileinfo">
								<li><a href="apprendreTeke.html">Leçon</a></li>	
                                <!--<li><a href="#">Audio</a></li>-->
								<li><a href="#">Quiz</a></li>
                                <li><a href="visite.html">visite guide</a></li>
                            </ul>
                        </li>
                        
                        
                        
                       <!-- <li><a href="apprendreTeke.html">Apprendre le TEKE</a>
                            <ul class="sousMenu">
                                <li><a href="#">Leçon</a></li>
                                <li><a href="#">visite Guidé</a></li>
                                <li><a href="#">Quiz</a></li>
                            </ul>
                        
                        </li>-->
                        <li><a href="evenement.html">Evenements</a></li>    
                        <li class="active"><a href="contact-us.php">Contact</a></li>    
                        <li><a href="formulaire.php">s'inscrire</a></li>    
                    </ul>
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->
		
    </header><!--/header-->
		
	<div class="map">
	</div>
	</div>
    <br>
	
	<section id="contact-page">
        <div class="container">
            <div class="center">        
                <h2>Contactez-nous</h2>
                <img src="images/Sans titre - 9.jpg" width="300" height="100">
<p class="lead"></p>
            </div> 
            <div class="row contact-wrap"> 
                <div class="col-sm-8 col-sm-offset-2">
                    <div id="sendmessage">Votre message a ete envoye. Merci!</div>
                    <div id="errormessage"></div>
                    <form action="" method="post" action="#" class="contactForm">
                        <div class="form-group">
                            <input type="text" name="nom" class="form-control" id="nom" placeholder="Votre Nom" required="" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                            <div class="validation"></div>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" name="email" id="email" placeholder="Votre Adresse Email" required="" data-rule="email" data-msg="Please enter a valid email" />
                            <div class="validation"></div>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="message" rows="5" data-rule="required" required="" data-msg="Please write something for us" placeholder="Votre Message"></textarea>
                            <div class="validation"></div>
                        </div>
                        
           <div class="text-center"><button type="submit" name="submit" class="btn btn-primary btn-lg">Envoyer</button></div>
                    </form>     
                </div>
            </div><!--/.row-->
        </div><!--/.container-->
    </section><!--/#contact-page-->

    <section id="bottom">
        <div class="container wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="widget">
                        
                    </div>    
                </div><!--/.col-md-3-->

                <div class="col-md-3 col-sm-6">
                    <div class="widget">
                        <h3> TSOUMOU </h3>
                        <img src="images/tsoumou.png" width="100" height="100">
                       <!-- <ul>
                            <li><a href="#">BP : 20 277-Libreville</a></li>
                            <li><a href="#">+24106 02 89 47 81</a></li>
                            <li><a href="#"> www.kaytechnologie.com </a></li>
                            <li><a href="#"> infos@kaytechnologie.com </a></li>                           
                        </ul>-->
                    </div>    
                </div><!--/.col-md-3-->

                <div class="col-md-3 col-sm-6">
                    <div class="widget">
                        <h4>Prestataire</h4>
                        <ul>
                            <li><a href="https://www.facebook.com/profile.php?id=100011192410168"><img src="images/logoKay.jpg" width="60" height="60">KAYTECHNOLOGIE</a></li>
                                                  
                        </ul>
                    </div>    
                    
                     <div class="widget">
                        <h5></h5>
                        <ul>
                            <li></a></li>                           
                        </ul>
                    </div>    
                </div><!--/.col-md-3-->
                
                
                 <div class="col-md-3 col-sm-6">
                    <div class="widget">
                        <h4>Partenaire</h4>
                        <ul>
                            <li><a href="https://www.facebook.com/allmultimediaservices/"><img src="images/allmultimediaservices%20logo1.jpg" width="60" height="50">Allmultimediaservice</a></li>
                                                  
                        </ul>
                    </div>    
                    
                     <div class="widget">
                        <h5></h5>
                        <ul>
                                                  
                        </ul>
                    </div>    
                </div><!--/.col-md-3-->

                <div class="col-md-3 col-sm-6">
                    <div class="widget">
                    </div>    
                </div><!--/.col-md-3-->
            </div>
        </div>
    </section><!--/#bottom-->
	
	<div class="top-bar">
		<div class="container">
			<div class="row">
			    <div class="col-lg-12">
				   <div class="social">
						<ul class="social-share">
							<li><a href="https://web.facebook.com/search/top/?q=camp%20numerique%20ngouoni"><i class="fa fa-facebook"></i></a></li>
						</ul>
				   </div>
                </div>
			</div>
		</div><!--/.container-->
	</div><!--/.top-bar-->	
	<footer id="footer" class="midnight-blue">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="credits">
                        <!-- 
                            All the links in the footer should remain intact. 
                            You can delete the links only if you purchased the pro version.
                            Licensing information: https://bootstrapmade.com/license/
                            Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Gp
                        -->
                   by Kaytechnologie
                    </div>
                </div>
                <div class="col-sm-6">
                    <ul class="pull-right">
                        
                        <li><a href="blog.html">J'apprends le TEKE</a></li>
                    
                    </ul>
                </div>
            </div>
        </div>
    </footer><!--/#footer-->
	
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/jquery.isotope.min.js"></script>   
    <script src="js/wow.min.js"></script>
	<script src="js/main.js"></script>
    
</body>
</html>